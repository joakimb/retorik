[inledning och definition]
Jag skulle vilja prata med er om basinkomst, även känt som medborgarlön. För de som inte känner till konceptet handlar det om att ge alla tillräckligt med pengar för att täcka behovskostnader såsom mat och hyra. Lite som om CSN bestämde sig för att de inte krävde att man studerade längre och de slopade återbetalningskravet. Ja, det är vad det betyder. Vi ska GE pengar till ALLA. Utan krav. Oavsett sysselsättning. Visst låter det bra?

[ethos]
Jag är snart färdig civilingenjör i datateknik, jag kommer inte att ha problem att hitta ett välbetalt jobb. Jag står inte här och talar för medborgarlön för att jag vill ha gratis pengar av staten, jag talar om det för att jag tror att vi som samhälle behöver detta.

[folk kommer att sluta jobba]
Basinkomst avfärdas ibland som en verklighetsfrånvänd vänsterutopi som bara hippies tror på. Jag menar, vem skulle jobba om alla hade gratis pengar? "Gratis pengar åt alla!", det låter för bra för att var sant. Samma sak har sagts om många andra stora ideer som vi tar för självklara idag. 8 timmars arbetsdag var en gång en utopi. Rösträtt åt kvinnor var en gång en utopi. Demokrati var en gång i tiden en utopi. Man ansåg att folket var för dumma och lata för att rösta. Känns det tankesättet igen? Kommer folk att sluta jobba om vi inför basinkomst? Det är klart att de inte kommer, precis som att folk inte är för dumma för att rösta.

Tänk efter. Hur många här skulle fortsätta jobba om ni hade 8000 kr i basinkomst? Tror ni att alla andra är så annorlund från er?