wanderer uver der me....
nån låt, (pink floyd?)
att dyka
äta nåt gott
surfa reddit
löpning
stress

Wanderer über dem nebelmeer

Han står där, i gryningen. Högt på berget. I brisen. Som en del av landskapet. Bakom honom, vandringen, strävan, dunklet. Framför honom, vandringen, strävan, ljuset. Han har gjort det. Han har trotsat landskapet, bergen, skogen. Han är herre över naturen och därmed sig själv. Ändå är han en del av helheten. Han står där, på toppen, i sin rock, färgad som skogen, med sin käpp, kommen ur skogen, och tänker just detta.

Där på toppen kan han se allt. Han kan se berget han står på. Hårt, brunt, kalt. Som ett oförstörbart torn utan herre. Inte illasinnat men inte välkomnande. Snarare som någonting enormt och oföränderligt med ett forntida ogreppbart syfte. Han har bestigit det. Men han har inte besegrat det, han samexisterar med det.

Han kan se längre. Han ser dimman, som omsluter landskapet. Den är skogens morgongäsp. Vit i gryningens ljus. En markör för mörkrets ände och dangens begynnelse. En övegång. En filt av flyktighet som söker dölja landskapet för solen. Som om landskapet inte ännu är redo för de starka strålarna. Även dimman är en del av helheten. Den är inte av ondo. Det finns inget gott eller ont, inga motsättningar. Bara naturen.

Han ser topparna, blåa i fjärran. De besegrar dimman, ty de är högre än den kan nå. De är uråldriga och kraftfulla. De fruktar inte solen, de längtar efter att bada i dagens ljus. Han är på väg dit. Han kan nå dit. Men först ska ha stå här. Han ska ta in allt. Han ska se helheten. Precis som morgondimmans stund är nutrens övergång till dagen, det ljusa, är samma stund vandrarens övergång. Det mörka ligger bakom. Natten och den första vandringen är över.

Nu kommer ljuset, dagen. Det samma natur, samma vandring, men i en annan skepnad. Det som kommer är lättare, välkomnande. Det är inte utan strävan, men det är inte skrämmande. Vandraren ska ta sig an landskapet. Han ska vara i det. Han är en del av det.

berget dunklt, andra sidan världen

grönheten

vitheten

tankarna


känslan

vinden

vad har hänt innan

finns det någon plan

symboliken
